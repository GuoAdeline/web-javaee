var newList = [];

function deleteLine(row){
	var d = row.parentNode.parentNode.rowIndex;
	document.getElementById('myTable').deleteRow(d);
}

// function deleteTheLine(o){
    // var p = o.parentNode.parentNode;
    // p.parentNode.removeChild(p);
// }

function getMyTDTag(ref){
	var theTDRef = ref;
	var found = false;
	while ((theTDRef !== null) && (! found)) {
		if (theTDRef.nodeType === 3 ){
			theTDRef = theTDRef.parentNode;
		} else if (theTDRef.tagName === "TD") {
			found = true;
		} else {
			theTDRef = theTDRef.parentNode;
		}
	}
	return theTDRef;
}

function getMyTRTag(ref){
	var theTRRef = ref;
	var found = false;
	while ((theTRRef !== null) && (! found)) {
		if (theTRRef.nodeType === 3 ){
			theTRRef = theTRRef.parentNode;
		} else if (theTRRef.tagName === "TR") {
			found = true;
		} else {
			theTRRef = theTRRef.parentNode;
		}
	}
	return theTRRef;
}

function changeBGToRed(ref){
	var row = getMyTDTag(ref);
	// row.parentNode.className = "backgroundRed";
	row.parentNode.style.backgroundColor="red";
}

function toClassInit(ref) {
	var row = getMyTDTag(ref);
	// row.parentNode.className = "line";
	row.parentNode.style.backgroundColor="transparent";
}

function changeBackground(ref, color) {
	var row = getMyTDTag(ref);
	row.parentNode.style.backgroundColor=color;
}

function refresh() {
	$.ajax({
		url:"/prwebJ2EE/refresh",
		method: "POST",
		success: function( result ) {
			removeLines();
                        addNewLines(result.data);
                        console.log("ok");
		},	
		error: function (resultat, statut, erreur){
			console.log("error");
		}
	});
}

//  JQuery methode
  function removeLines() {
	  $(".line").remove();
  }
  
  function addNewLines(result) {
      var listeTR = document.getElementsByTagName("TR");
      if (listeTR.length >= 1) {
          console.log(result);
          for (var i=0; i < result.length; i++) {
              var line = result[i];
              addNewLine(line.id, line.author, line.title, line.body, line.category_name);
          }
      }
  }
  
  function addNewLine(id, author, title, subject, category_name) {
	 var string="<tr class=\"line\">";
	 string += "<td>"+id+"</td>";
	 string += "<td>"+title+"</td>";
	 string += "<td>"+author+"</td>";
	 string += "<td>"+subject+"</td>";
         string += "<td>"+category_name+"</td>";
	 string += '<td><input type="button" name="delete" value="delete" onclick="deleteLine(this, ${ auction.id });"/></td>';
         string += "</tr>";
	 
	 $('#myTable tr:last').before(string);
 }
 
 function deleteLine(theRef, id) {
     $.ajax({
         url:"/prwebJ2EE/delete",
         data:{
             "id":id
         },
         method: "POST",
         success: function( result ) {
             var theTR = getMyTRTag(theRef);
             if (theTR !== null) {
                 var parent = theTR.parentNode;
                 parent.removeChild(theTR);
             }
         },
         error: function (result, statut, erreur) {
             console.log("erreur");
         }
     });
 }


// var list;
// var new_list;

// function deleteIdList(){
	// list = [];
	// list.push(document.getElementById('deleteId').value);
	// var num;
	// num = document.getElementById('deleteId').value;
	// if (num>0 && num<7){
	// list.push(num);
	// } else {
	// alert("Number of id is wrong!");
	// }
	// new_list=list.join(",");
	// var list=[];
	// var obj=document.getElementById("deleteId");
	// for(var i=0;i<6;i++){
		// if(obj.value>0){
			 // list.push(obj.value);
		// }
	// }
	// alert(list);
// }






// DOM methode 
//function removeLines() {
//	var listeTR = document.getElementsByTagName("TR");
//	var toBeRemoved = new Array();
//	for (var i=0; i < listeTR.length; i++) {
//		var ref = listeTR[i];
//		if (ref.className === "line") {
//			toBeRemoved.push(ref);
//		}
//	}
//	while (toBeRemoved.length > 0) {
//		ref = toBeRemoved.pop();
//		ref.parentNode.removeChild(ref);
//	}
//}
 