<%-- 
    Document   : index
    Created on : 2019-2-25, 16:31:31
    Author     : GUO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Auctions list</title>
        <meta charset=UTF-8"/>
        <link rel="stylesheet" type="text/css" media="screen" href="main.css">
        <script src="main.js" type="text/javascript"></script>
        <script type="text/javascript" language="javascript" src="jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <jsp:useBean id="auction" scope="page" class="org.centrale.prweb.item.Auction" ></jsp:useBean>
        <h1>List of items</h1>
        <table id="myTable">
            <tr>
                <th>Auction #</th>
                <th>Auction Type</th>
                <th>Seller</th>
                <th>Description</th>
                <th>Category</th>
                <th><input type="button" name="refresh" value="refresh" onclick="refresh();"/></th>
            </tr>
            <c:forEach items="${ auctions }" var="auction">
                <tr class="line">
                    <td>${ auction.id }</td>
                    <td>${ auction.title }</td>
                    <td>${ auction.author }</td>
                    <td>${ auction.body }</td>
                    <td><c:if test="${ auction.category != null }">${ auction.category.name }</c:if></td>
                    <td><input type="button" name="delete" value="delete" onclick="deleteLine(this, ${ auction.id });" /></td>
                </tr>
            </c:forEach>
            <form action="add" method="POST">
                <tr id="addNew">
                    <td></td>
                    <td><input type="text" name="title" id="title" size="20" style="background-color: lightgrey;"/></td>
                    <td><input type="text" name="author" id="author" size="20" style="background-color: lightgrey;"/></td>
                    <td><input type="text" name="body" id="body" size="60" style="background-color: lightgrey;"/></td>
                    <td><select name="category">
                            <option value="-1">-</option>
                            <c:forEach items="${ categories }" var="category">
                            <option value="${ category.id }">${ category.name }</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td style="text-align: center"><button>add</button></td>
                </tr>
            </form>
        </table>
    </body>
</html>
